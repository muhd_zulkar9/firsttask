import csv, sqlite3, sys

file_input = sys.argv[1]
db_name = sys.argv[2]

if not ".csv" in file_input:
  file_input += ".csv"
  
if not ".db" in db_name:
    db_name += ".db"


sql_command = """INSERT OR IGNORE INTO testing_table(no_id, first_name, last_name, email, gender, ip_address) 
VALUES(:no_id, :first_name, :last_name, :email, :gender, :ip_address)"""
#cursor.execute(sql_command)

with open (file_input, 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    #for row in csv_reader:
        #print(row)

    with sqlite3.connect(db_name) as conn:
        cursor = conn.cursor()
        
        #CREATE TABLE
        SQL = """ CREATE TABLE IF NOT EXISTS testing_table (
        "no_id" TEXT,
        "first_name" TEXT,
        "last_name" TEXT,
        "email" TEXT,
        "gender" TEXT,
        "ip_address" REAL, 
        primary key(no_id)
        );"""
        cursor.execute(SQL)

        cursor.executemany(sql_command, csv_reader)





