**HOW TO RUN**

Run "io.py" to import data ".CSV FILE to SQLITE"
Run "dbtocsv.py" to import data "SQLITE to .CSV FILE"

**ABOUT THE CODING**
 
**CSV TO SQLITE**
For converting data from to sqlite, first I connect to the database called "testing.db" 
and created table called testingTable using sql_command.

Then parse the csv data from "MOCK_DATA.csv" to saved it into sqlite.

For converting data from csv to sqlite, its better to use csv instead of row.split as it 
can cause an error when other delimeter added to the csv file.

Then insert the data into the table and close the connection.

**SQLITE TO CSV**
First I connected to the database called "testing.db" which already contains table
called "testingTable"full of data.

Then use ("select * from testingTable") to pull the data.

Then parse the data to csv file called "MOCK_DATA2.csv" and close the connection.



