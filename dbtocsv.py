import csv, os, sys
import sqlite3

file_input = sys.argv[1]
db_name = sys.argv[2]

if not ".csv" in file_input:
  file_input += ".csv"

if not ".db" in db_name:
  db_name += ".db"

connection = sqlite3.connect(db_name)
cursor = connection.cursor()

if os.path.exists(file_input):
  os.remove(file_input)
else:
  print("The {} file does not exist".format(file_input))
  print("Creating {}".format(file_input))


#VIEW DATA
#print ("--DATA--")
#cursor.execute('''SELECT * FROM testingTable''')
#rows = cursor.fetchall()

#for row in rows:
#   print(row)

#DB TO CSV
print ("DB TO CSV")
cursor.execute("select * from testing_table")

with open(file_input, mode="w", newline='') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=",", quotechar='"')
    csv_writer.writerow([i[0] for i in cursor.description])
    csv_writer.writerows(cursor)
    connection.commit()

#CLOSE CONNECTION
print ("SUCCESS")
connection.close()

